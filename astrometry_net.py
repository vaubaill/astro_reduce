"""


"""
import os
import glob
from astropy.table import Table
from astroquery.astrometry_net import AstrometryNet
from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u

def run_astrometrynet(imgfile,rawdir='RAW',oridir='ORIGINAL',sexres='SEXRES'):
    """Run astrometry.net for image file.
    
    Parameters
    ----------
    
    
    Returns
    -------
    
    
    """
    if not os.path.exists(sexres):
        os.mkdir(sexres)
    strfile = imgfile.replace('.fits','.dat').replace(rawdir,sexres)
    in_pth = os.path.dirname(imgfile)
    outpth = in_pth.replace(rawdir,oridir)
    if not os.path.exists(outpth):
        os.mkdir(outpth)
    outfile = imgfile.replace(in_pth,outpth)
    
    cmd = 'sex '+imgfile+\
        '  -c /home/vaubaill/.local/data/t120.sex -PARAMETERS_NAME /home/vaubaill/.local/t120.param -FILTER_NAME /home/vaubaill/.local/data/default.conv -CATALOG_NAME '+\
        strfile+' -CATALOG_TYPE ASCII_HEAD'
    print(cmd)
    os.system(cmd)
    
    
    # read Sextractor output
    sources = Table.read(strfile,format='ascii.sextractor')
    sources.sort('FLUX_APER')
    sources.reverse()
    
    # read image
    print('Now reading '+imgfile)
    hdu     = fits.open(imgfile )
    header = hdu[0].header
    data    = hdu[0].data
    hdu.close()
    (image_width,image_height) = data.shape
    skycoo_i = SkyCoord(ra=header['OBJCTRA'],dec=header['OBJCTDEC'],
                        unit=(u.hourangle, u.deg), frame="icrs")
    print('T120 RA/DEC: '+skycoo_i.to_string(style='hmsdms'))
    radius = 12.0/60.0
    
    ast = AstrometryNet()
    ast.api_key = 'ibluryspyzhcbyny'
    try:
        wcs_header = ast.solve_from_source_list(sources['X_IMAGE'],
                                            sources['Y_IMAGE'],
                                            image_width,
                                            image_height,
                                            solve_timeout=120,
                                            scale_est=0.77,
                                            scale_units='arcsecperpix',
                                            scale_type='ev',
                                            scale_err=5.0,
                                            #center_ra=skycoo_i.ra.value,
                                            #center_dec=skycoo_i.dec.value,
                                            #radius=radius,
                                            parity=2)
        
        #wcs_header = ast.solve_from_image(imgfile, force_image_upload=True)
    except:
        print('*** FATL ERROR: Unable to solve astrometry for file '+imgfile)
        return
    skycoo_f = SkyCoord(ra=wcs_header['CRVAL1']*u.deg,dec=wcs_header['CRVAL2']*u.deg, frame="icrs")
    print('True RA/DEC: '+skycoo_f.to_string(style='hmsdms'))
    print('Separation : '+str(skycoo_f.separation(skycoo_i).to('arcmin')))
    # update header
    for hdrkey in header[6:]:
        try:
            wcs_header[hdrkey]
        except:
            wcs_header[hdrkey] = header[hdrkey]
    
    # save in output file
    hdunew = fits.PrimaryHDU(data,header=wcs_header)
    hdunew.writeto(outfile,overwrite=True)
    print('file saved in '+outfile)

if __name__ == '__main__':
    
    listimg = glob.glob('./RAW/*.fits')
    for imgfile in listimg:
        run_astrometrynet(imgfile)
        
else:
    print('successfully imported')
